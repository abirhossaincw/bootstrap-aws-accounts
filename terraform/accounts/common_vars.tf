# Common variables

variable "organization_name" {
  type = string
  default = "abc"
}

variable "aws_default_region" {
  type = string
  default = "ap-southeast-2"
}

variable "keybase_username" {
  type = string
  default = "abc"
}

variable "iam_user_password_length" {
  type = number
  default = 25
}

variable "iam_group_administrators_name" {
  type = string
  default = "administrators"
}

variable "iam_user_admin_username" {
  type = string
  default = "admin"
}

variable "iam_user_terraform_username" {
  type = string
  default = "terraform"
}

variable "iam_role_terraform_name" {
  type = string
  default = "terraform"
}

variable "iam_role_staff_read_only_name" {
  type = string
  default = "staff-read-only"
}

variable "abc_accounts" {
  type = map(object({
    account_id = number,
    account_name = string,
    account_email = string
  }))

  default = {
    shared_services = {
      account_id = XXXXXXXXXXXXX
      account_name = "ABC Shared Services"
      account_email = "aws_shared_services_root@abc.com"
    },
    qa = {
      account_id = XXXXXXXXXXXXX
      account_name = "ABC QA"
      account_email = "aws_qa_root@abc.com"
    },
    staging = {
      account_id = XXXXXXXXXXXXX
      account_name = "ABC Staging"
      account_email = "aws_staging_root@abc.com"
    }
    production = {
      account_id = XXXXXXXXXXXXX
      account_name = "ABC Production"
      account_email = "aws_production_root@abc.com"
    }
  }
}

# Account variables
variable "account_id" {
  type = number
}

variable "account_name" {
  type = string
}

variable "account_email" {
  type = string
}

variable "account_alias" {
  type = string
}