output "iam_user_admin_keybase_password_pgp_message" {
  value = module.iam_user_admin.keybase_password_pgp_message
}

output "iam_user_admin_keybase_secret_key_pgp_message" {
  value = module.iam_user_admin.keybase_secret_key_pgp_message
}

output "iam_user_abir_hossain_keybase_password_pgp_message" {
  value = module.iam_user_abir_hossain.keybase_password_pgp_message
}

output "iam_user_abir_hossain_keybase_secret_key_pgp_message" {
  value = module.iam_user_abir_hossain.keybase_secret_key_pgp_message
}

output "iam_user_amine_bensaidi_keybase_password_pgp_message" {
  value = module.iam_user_amine_bensaidi.keybase_password_pgp_message
}

output "iam_user_amine_bensaidi_keybase_secret_key_pgp_message" {
  value = module.iam_user_amine_bensaidi.keybase_secret_key_pgp_message
}

output "iam_user_charles_batshon_keybase_password_pgp_message" {
  value = module.iam_user_charles_batshon.keybase_password_pgp_message
}

output "iam_user_charles_batshon_keybase_secret_key_pgp_message" {
  value = module.iam_user_charles_batshon.keybase_secret_key_pgp_message
}

output "iam_user_terraform_keybase_password_pgp_message" {
  value = module.iam_user_terraform.keybase_password_pgp_message
}

output "iam_user_terraform_keybase_secret_key_pgp_message" {
  value = module.iam_user_terraform.keybase_secret_key_pgp_message
}