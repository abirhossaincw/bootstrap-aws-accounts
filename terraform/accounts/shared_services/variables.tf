variable "iam_group_developers_name" {
  default = "developers"
}

variable "iam_group_testers_name" {
  default = "testers"
}

variable iam_users_usernames {
  default = {
    "abir_hossain": "abir.hossain@abc.com",
    "amine_bensaidi": "amine.bensaidi@abc.com",
    "charles_batshon": "charles.batshon@abc.com"
  }
}

variable iam_cicd_user_username {
  default = "terraform"
}