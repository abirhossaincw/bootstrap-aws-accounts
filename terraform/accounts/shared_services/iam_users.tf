# IAM user - 'admin'
module "iam_user_admin" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-user"
  version = "~> 2.0"

  name            = var.iam_user_admin_username
  pgp_key         = "keybase:${var.keybase_username}"
  password_length = var.iam_user_password_length

  password_reset_required = false
}

module "iam_user_abir_hossain" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-user"
  version = "~> 2.0"

  name            = var.iam_users_usernames.abir_hossain
  pgp_key         = "keybase:${var.keybase_username}"
  password_length = var.iam_user_password_length

  password_reset_required = true
}

module "iam_user_amine_bensaidi" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-user"
  version = "~> 2.0"

  name            = var.iam_users_usernames.amine_bensaidi
  pgp_key         = "keybase:${var.keybase_username}"
  password_length = var.iam_user_password_length

  password_reset_required = true
}

module "iam_user_charles_batshon" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-user"
  version = "~> 2.0"

  name            = var.iam_users_usernames.charles_batshon
  pgp_key         = "keybase:${var.keybase_username}"
  password_length = var.iam_user_password_length

  password_reset_required = true
}

module "iam_user_terraform" {
  source  = "terraform-aws-modules/iam/aws//modules/iam-user"
  version = "~> 2.0"

  name            = var.iam_cicd_user_username
  pgp_key         = "keybase:${var.keybase_username}"
  password_length = var.iam_user_password_length

  password_reset_required = false
}