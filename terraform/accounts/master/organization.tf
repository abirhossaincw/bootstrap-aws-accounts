resource "aws_organizations_organization" "org" {
  feature_set = "ALL"

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_organizations_account" "shared_services" {
  name      = var.abc_accounts.shared_services.account_name
  email     = var.abc_accounts.shared_services.account_email

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_organizations_account" "qa" {
  name      = var.abc_accounts.qa.account_name
  email     = var.abc_accounts.qa.account_email

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_organizations_account" "staging" {
  name      = var.abc_accounts.staging.account_name
  email     = var.abc_accounts.staging.account_email

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_organizations_account" "production" {
  name      = var.abc_accounts.production.account_name
  email     = var.abc_accounts.production.account_email

  lifecycle {
    prevent_destroy = true
  }
}